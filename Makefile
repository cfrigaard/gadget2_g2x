# Copyright 2009-13- Carsten Eie Frigaard.
#
# License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.

# ic data root file

DAT=b_16
#DAT=galaxy
#DAT=lcdm_gas

PARAM=Dat/ana_$(DAT).param

# number of mpi cpu's to use
NCPUS=1

# hostfile, used only if you add -machinefile $(HOSTFILE) to mpirun below
HOSTFILE=hosts.txt

# run a gpuprof performance profile
# export gpuprof=1

# Commandline argument parsing
#   DO not change these...begin

ifeq ($(dbg),1)
	BINSUFFIX=.d
	TARGET=debug
else
	TARGET=release
endif
ifeq ($(emu),1)
	EMUSUFFIX=emu
	TARGET=emu$(TARGET)
endif
ifeq ($(cuda),0)
	CUDA=-cuda $(cuda)
endif
ifeq ($(cuda),1)
	CUDA=-cuda $(cuda)
endif
ifeq ($(cuda),2)
	CUDA=-cuda $(cuda)
endif
ifeq ($(cudadebug),0)
	CUDA=$(CUDA)-cudadebug $(cudadebug)
endif
ifeq ($(cudadebug),1)
# 	CUDA=$(CUDA)-cudadebug $(cudadebug)
endif
ifeq ($(cudadebug),2)
	CUDA=$(CUDA)-cudadebug $(cudadebug)
endif
ifeq ($(cudadebug),3)
	CUDA=$(CUDA)-cudadebug $(cudadebug)
endif
ifeq ($(cudadebug),4)
	CUDA=$(CUDA)-cudadebug $(cudadebug)
endif

#   DO not change these...end

# Directory and file dependency setup
CUDADIR      =Src/Gadget_cuda_gx
GADGETDIR    =Src/Gadget-2.0.5/Gadget2
OUTDIR       =Out/Out_$(DAT)

CUDAFILES    =$(wildcard $(CUDADIR)/*.cu $(CUDADIR)/*.cpp $(CUDADIR)/*.h)
GADGETFILES  =$(wildcard $(GADGETDIR)/*.c $(GADGETDIR)/*.cpp $(GADGETDIR)/*.h)
XDEPEND      =Makefile $(CUDADIR)/Makefile $(GADGETDIR)/Makefile $(GADGETDIR)/gadget.options

BIN          =$(EMUSUFFIX)Gadget2$(BINSUFFIX)
MPIRUN_BASE  =mpiexec -machinefile $(HOSTFILE) -n $(NCPUS)
#MPIRUN_BASE  = ccc_mprun -x -n $(NCPUS) -c 1 -p hybrid 
MPIRUN_BIN   =Bin/$(BIN) $(PARAM) $(CUDA)
MPIRUN       =$(MPIRUN_BASE) $(MPIRUN_BIN)
MPIRUN_FILTER=| tee out.$(DAT).txt | grep -e "\#\#" -e "\[" -e "\*\*"

# Building the executables
Bin/$(BIN): $(CUDAFILES) $(GADGETFILES) $(XDEPEND)
	@ $(MAKE) -s -C $(CUDADIR)
	@ $(MAKE) -s -C $(GADGETDIR)
	@ cp $(GADGETDIR)/$(EMUSUFFIX)Gadget2$(BINSUFFIX) $@

DATFILE=Dat/$(shell echo \"$(DAT)\" | sed s/_[0-9]*\"/\"/g | sed s/\"//g ).options
$(GADGETDIR)/gadget.options: $(DATFILE) Makefile
	@ test -f $(DATFILE) || echo WARNING: file $(DATFILE) does not exist...ignoring gadget.options copy
	@ ! test -f $(DATFILE) \
	 || (diff -dwq $(DATFILE) $@ \
	 || echo COPYING: $(DATFILE)  to  $@ \
	 && cp $(DATFILE) $@)
	@ touch $@
	 
# Running under mpd
.PHONY: run
run: Bin/$(BIN) pre_run	
	@ echo RUN: $(MPIRUN) 
	@ $(MPIRUN) $(MPIRUN_FILTER)
	@ $(MAKE) -s post_run

.PHONY: pre_prun
pre_run: tarcode.tgz
	@ - mkdir -p Out/Out_$(DAT)
	@ - rm -f Out/Out_$(DAT)/snapshot_* 2>/dev/null
	@ mv tarcode.tgz Out/Out_$(DAT)
	@ cp cudakernelsize.txt hosts.txt Makefile $(GADGETDIR)/gadget.options $(CUDADIR)/options.h Out/Out_$(DAT) 
	@ cp $(GADGETDIR)/Makefile Out/Out_$(DAT)/Makefile.gadget2
	@ cp $(CUDADIR)/Makefile   Out/Out_$(DAT)/Makefile.g2x
	
.PHONY: post_run
post_run:
	@ - rm Out/Out_$(DAT)/restart.* -f 2>/dev/null
	@ cp out.$(DAT).txt $(OUTDIR)/out.txt

# run with performance lib
GPUPROFRUN=gpuprof -d 0 $(MPIRUN_BIN)
.PHONY: run_gpuprof 
run_gpuprof: Bin/$(BIN) pre_run
ifneq ($(gpuprof),1)
	@ echo ERROR: environment 'gpuprof' not setted in Makefile && false
endif
	@ echo RUN: $(GPUPROFRUN)
	@ $(GPUPROFRUN) 2>out.1.$(DAT).txt $(MPIRUN_FILTER) >out.2.$(DAT).txt
	@ $(MAKE) -s post_run
	@ #cat out.2.$(DAT).txt | grep "p=" >out.3.$(DAT).txt

# Testing the gadget output,
#   needs a .g suffix on all snapshotfiles (patched into the gadget code)
#   also needs gdiff, included in the Gadget_addon_tools ()
#   Ref='cuda=0', Cuda1='cuda=1' Cuda2='cuda=2', Cuda2Emu='cuda=2 emu=1'
#
#REFDIR=cuda2-ncpu$(NCPUS)
#TARDIR=$(OUTDIR)
#
#OUTFILES=$(wildcard $(TARDIR)/*_???.g)
#ASCFILES=$(OUTFILES:.g=.asc)
#DIFFILES=$(OUTFILES:.g=.diff)
#REFFILES=$(OUTFILES:.g=.ref)
#
#DISTFILES=$(OUTFILES:.g=.dist)
#REFFILES=$(OUTDIR)/$(REFDIR)/$(notdir $(OUTFILES:.g=.ref))
#
#.NOTPARALLEL: %.dist
#%.dist: %.g Makefile
#	@ echo -n gdiff $< $(OUTDIR)/$(REFDIR)/$(notdir $<) "  "
#	@ gdiff -r  $< $(OUTDIR)/$(REFDIR)/$(notdir $<) > $@
#	@ tail -n 1 $@
#
#.NOTPARALLEL: %.ref
#%.ref: %.dist Makefile
#	@ echo -n make ref $< "  " $@
#	@ gdiff -r $@ $(OUTDIR)/$(REFDIR)/$(notdir $@)
#
#.NOTPARALLEL: dist
#dist: $(DISTFILES)
#
#.NOTPARALLEL: fdist
#fdist:
#	@ rm -f $(DISTFILES)
#	@ $(MAKE) -s dist
#
#.NOTPARALLEL: ref
#ref: $(REFFILES)
#
#.NOTPARALLEL: mkref
#mkref:
#	mkdir -p                $(OUTDIR)/$(REFDIR)
#	cp -f $(OUTDIR)/out.txt $(OUTDIR)/$(REFDIR)
#	cp -f $(OUTFILES)       $(OUTDIR)/$(REFDIR)
#	- cp -f $(DISTFILES)    $(OUTDIR)/$(REFDIR)

CURREV=$(shell svn info | grep "Revision: " | sed s/Revision:\ //)
TAREXCLUDE=--exclude="*.tgz" --exclude="g2x/out*" --exclude "g2x/Temp" --exclude="g2x/Out/*" --exclude="*~" --exclude=".svn*" --exclude="g2x/Dat/ic_b_0[5-9].g" --exclude="g2x/Dat/ic_b_1?.g" --exclude="g2x/Bin/*" --exclude="*/gpuprof.h" --exclude="*.\#*"
ifeq ($(smalltar),1)
	TAREXCLUDE+= --exclude="Dat" 
endif
tar: clean
	tar czf g2x-0.$(CURREV).tgz ../g2x $(TAREXCLUDE)

.PHONY:tarcode.tgz
tarcode.tgz:
	@ tar czf tarcode.tgz $(CUDAFILES) $(GADGETFILES) $(XDEPEND)

# cleaning
.PHONY:clean
clean:
	@ $(MAKE) -s -C $(CUDADIR)   clean
	@ $(MAKE) -s -C $(GADGETDIR) clean
	@ $(MAKE) -s -C $(CUDADIR)   clean dbg=1
	@ $(MAKE) -s -C $(GADGETDIR) clean dbg=1
	@ $(MAKE) -s -C $(CUDADIR)   clean emu=1
	@ $(MAKE) -s -C $(GADGETDIR) clean emu=1
	@ $(MAKE) -s -C $(CUDADIR)   clean emu=1 dbg=1
	@ $(MAKE) -s -C $(GADGETDIR) clean emu=1 dbg=1
	@ rm Bin/$(BIN) Bin/emuGadget2 Bin/Gadget2.d Bin/emuGadget2.d testsuite.txt gindex.txt out.txt out.*.txt -f 2>/dev/null
