// Copyright 2009 Carsten Eie Frigaard.
//
// License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
// This is free software: you are free to change and redistribute it.
// There is NO WARRANTY, to the extent permitted by law.
//
// This software is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CUDACHECKMINI_H__
#define __CUDACHECKMINI_H__

// Check floats: mini test/assert functionality
//#define  CUDA_USE_CHECK_MINI_GX

#ifdef CUDA_USE_CHECK_MINI_GX

__device__ static int               g_sig_line_cuda=0;
__device__ static FLOAT_INTERNAL_GX g_sig_x_cuda=0;
__device__ static char              g_sig_msg_cuda[1024];

__device__ void check_mini_float_gx(const FLOAT_INTERNAL_GX x,const char*const file,const int line)
{
	int r=0;
	const FLOAT_INTERNAL_GX y=x<0 ? -x : x;
	if (y < 1E-20 && y!=0) r=1;
	if (y > 1E10) r=2;
	if (!(x==x)) r=3;
	
	if (r!=0 && g_sig_line_cuda==0) {
		g_sig_line_cuda=line;
		g_sig_x_cuda=x;
		for(int i=0;i<1024 && file[i]!=0;++i) g_sig_msg_cuda[i]=0;
		if (file) {
			for(int i=0;i<1024 && file[i]!=0;++i) g_sig_msg_cuda[i]=file[i];
		}
	}	
}

int check_mini_init()
{
	int l=0;
	if (cudaSuccess!=cudaMemcpyToSymbol(g_sig_line_cuda,&l,sizeof(l))) ERROR("error in cudaMemcpyToSymbol");
	return 0;
}

int check_mini_finalize()
{
	int l=0;
	if (cudaSuccess!=cudaMemcpyFromSymbol(&l,g_sig_line_cuda,sizeof(l))) ERROR("error in cudaMemcpyToSymbol");
	if (l!=0) {
		FLOAT_INTERNAL_GX x=0;
		char f[1024];

		if (cudaSuccess!=cudaMemcpyFromSymbol(&x,g_sig_x_cuda,sizeof(x))) ERROR("error in cudaMemcpyToSymbol");
		if (cudaSuccess!=cudaMemcpyFromSymbol(&f,g_sig_msg_cuda,sizeof(f))) ERROR("error in cudaMemcpyToSymbol");


		fprintf(stderr,"line=%d, x=%g, file=%s",l,x,f);
		ERROR("my assert");
		return 1;
	}
	return 0;
}

#define CHECK_FLOAT_GX(x) check_mini_float_gx(x,__FILE__,__LINE__)

#else 

#define check_mini_init()
#define check_mini_finalize()
#define CHECK_FLOAT_GX(x)

#endif	
#endif