# !/bin/sh

declare OUT=ic_output.txt
declare ic_parms="../../Dat/pk.dat out.ic -h 0.72 -omega_m 0.3 -omega_l 0.7"

function GENIC {
	echo '**' Generating on $6 begin  | tee -a $OUT
	echo '** cmd=ic' $1 $2 $3 $4 $5 $ic_parms $8 $9 $10 | tee -a $OUT
	nice ic $1 $2 $3 $4 $5 $ic_parms $8 $9 $10 | tee -a $OUT
	echo | tee -a $OUT
	echo | tee -a $OUT
	echo '** cmd=tpm2gadget out.ic ../../Dat/'$6 $7 | tee -a $OUT
	nice tpm2gadget out.ic ../../Dat/$6 $7 | tee -a $OUT
	echo | tee -a $OUT
	echo '**' Generating on $6 end | tee -a $OUT
}

echo Generating ics... | tee $OUT

# # testing boxsize dependency
# GENIC 1  32 64  10  49 ic_a_01.g
# GENIC 1  32 64  50  49 ic_a_02.g
# GENIC 1  32 64 100  49 ic_a_03.g
# GENIC 1  32 64 200  49 ic_a_04.g
#

# # testing particle dependency
#GENIC 1   8 64  50  49 ic_b_01.g
#GENIC 1  16 64  50  49 ic_b_02.g
#GENIC 1  32 64  50  49 ic_b_03.g
#GENIC 1  48 64  50  49 ic_b_04.g
#GENIC 1  64 64  50  49 ic_b_05.g
#GENIC 1  80 64  50  49 ic_b_06.g
#GENIC 1  96 64  50  49 ic_b_07.g
#GENIC 1 112 64  50  49 ic_b_08.g
#GENIC 1 128 64  50  49 ic_b_09.g
#GENIC 1 144 64  50  49 ic_b_10.g
#GENIC 1 160 64  50  49 ic_b_11.g
#GENIC 1 192 64  50  49 ic_b_12.g
#GENIC 1 208 64  50  49 ic_b_13.g
#GENIC 1 224 64  50  49 ic_b_14.g
#GENIC 1 240 64  50  49 ic_b_15.g
#GENIC 1 256 64  50  49 ic_b_16.g
#GENIC 1 384 64  50  49 ic_b_17.g
# ic segfaults above around 425, due to overflow in int
#GENIC 1 512 64  50  49 ic_b_18.g
#GENIC 1 768 64  50  49 ic_b_19.g
#GENIC 1 1024 64 50  49 ic_b_20.g

# Generate some gas examples
GENIC 1  32 64  50  49 ic_g_03.g -gas
GENIC 1  48 64  50  49 ic_g_04.g -gas
GENIC 1  64 64  50  49 ic_g_05.g -gas
#GENIC 1  80 64  50  49 ic_g_06.g -gas
#GENIC 1  96 64  50  49 ic_g_07.g -gas
#GENIC 1 112 64  50  49 ic_g_08.g -gas
#GENIC 1 128 64  50  49 ic_g_09.g -gas
#GENIC 1 144 64  50  49 ic_g_10.g -gas
#GENIC 1 160 64  50  49 ic_g_11.g -gas
#GENIC 1 192 64  50  49 ic_g_12.g -gas
#GENIC 1 208 64  50  49 ic_g_13.g -gas
#GENIC 1 224 64  50  49 ic_g_14.g -gas
#GENIC 1 240 64  50  49 ic_g_15.g -gas
#GENIC 1 256 64  50  49 ic_g_16.g -gas
#GENIC 1 384 64  50  49 ic_g_17.g -gas

#
# # testing redshifdependence
# GENIC 1 32 64   50   1 ic_c_01.g
# GENIC 1 32 64   50   9 ic_c_02.g
# GENIC 1 32 64   50  19 ic_c_03.g
# GENIC 1 32 64   50  49 ic_c_04.g
# GENIC 1 32 64   50  99 ic_c_05.g
# GENIC 1 32 64   50 199 ic_c_06.g
#
# # real data generation using ic
# GENIC 1 64 64  100 49 ic_d_01.g
# GENIC 1 64 64  100 49 ic_d_02.g -n -poisson
# GENIC 1 64 64  100 49 ic_d_03.g -n -nozeldo
# GENIC 1 64 64  100 49 ic_d_04.g -n -poisson -nozeldo

# real data 2 - 50 Mpc bbox
# # 01 : 64^3=262144 particles, halo
# GENIC 1 64 64 50 49 ic_e_01.g
# # 02 : 64^3=262144 particles, half halo, half dm (-r = split opt, rand distrib)
# GENIC 1 64 64 50 49 ic_e_02.g -r
# # 03 : 128^3=2097152 particles, halo
# GENIC 1 128 128 50 49 ic_e_03.g
# # 04 : 128^3=2097152 particles, half halo, half dm (-r = split opt, rand distrib)
# GENIC 1 128 128 50 49 ic_e_04.g -r

# 05 : extreme case 256^3 particles, half halo, half dm (-r = split opt, rand distrib)
# GENIC 1 256 256 50 49 ic_e_05.g -r

# f: test of Sirko Ic vs 2lpt ic, pretty normal setup:

# # real data 3 - 50 Mpc bbox
# # 01 : 64^3=262144 particles, dm
# GENIC 1  64  64 50 49 ic_f_01_sirko.g -dm
# # 02 : 128^3=2097152 particles, dm
# GENIC 1 128 128 50 49 ic_f_02_sirko.g -dm
# # real data 3 - 100 Mpc bbox
# # 01 : 64^3=262144 particles, dm
# GENIC 1  64  64 100 49 ic_f_03_sirko.g -dm
# # 02 : 128^3=2097152 particles, dm
# GENIC 1 128 128 100 49 ic_f_04_sirko.g -dm
#
# # generate ensemble
# GENIC 2342     64  64 50 49 ic_f_01_sirko-ensemble_00.g -dm
# GENIC 435457   64  64 50 49 ic_f_01_sirko-ensemble_01.g -dm
# GENIC 921      64  64 50 49 ic_f_01_sirko-ensemble_02.g -dm
# GENIC 91632    64  64 50 49 ic_f_01_sirko-ensemble_03.g -dm
# GENIC 53201    64  64 50 49 ic_f_01_sirko-ensemble_04.g -dm
# GENIC 165066   64  64 50 49 ic_f_01_sirko-ensemble_05.g -dm
# GENIC 38105    64  64 50 49 ic_f_01_sirko-ensemble_06.g -dm
# GENIC 968263   64  64 50 49 ic_f_01_sirko-ensemble_07.g -dm
# GENIC 3201     64  64 50 49 ic_f_01_sirko-ensemble_08.g -dm
# GENIC 6291302  64  64 50 49 ic_f_01_sirko-ensemble_09.g -dm

rm -f out.ic.px out.ic.py out.ic.pz out.ic.vx out.ic.vy out.ic.vz | tee -a $OUT
echo '** All OK' | tee -a $OUT


